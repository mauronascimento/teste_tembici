from flask import Flask, request, jsonify, Response
from flask_caching import Cache
from view.tabuleiro_view import Tabuleiro
import logging
import json

config = {
    "DEBUG": True,
    "CACHE_TYPE": "simple",
    "CACHE_DEFAULT_TIMEOUT": 300
}
app = Flask(__name__)
app.config.from_mapping(config)
cache = Cache(app)
tabuleiro = Tabuleiro()


@app.route('/', methods=['GET'])
def init():
    return 'Teste Tembici'


@app.route('/info', methods=['GET'])
def info():

    logging.debug('Starting processing')
    jogo = tabuleiro.iniciar()
    return jsonify(jogo)


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    app.run(debug=True, host='0.0.0.0')