import random
import logging
from .impulsivo_view import Impulsivo
from .aleatorio_view import Aleatorio
from .exigente_view import Exigente
from .cauteloso_view import Cauteloso
from .propriedade_view import Propriedade

class Tabuleiro:

    def __init__(self):
        self.vencedor = None
        self.propriedades = [
            Propriedade('Alphaville', 300, 100),
            Propriedade('Jandira', 200, 50)
        ]
        self.jogadores = [
            Impulsivo('Impulsivo'),
            Aleatorio('Aleatorio'),
            Exigente('Exigente'),
            Cauteloso('Cauteloso')
        ]

    def iniciar(self):

        retorno= []
        for rodada in range(0, 300):
            for vezDoJogador in filter(lambda jogador: not jogador.estaFalido(), self.jogadores):
                posicao = self.__rodarDado()
                vezDoJogador.pularPosicao(posicao, len(self.propriedades))
                propriedade = self.propriedades[vezDoJogador.posicao]
                if self.temQuePagarAluguel(propriedade, vezDoJogador):
                    self.pagarAluguel(propriedade, vezDoJogador)
                elif vezDoJogador.deveComprar(propriedade):
                    self.comprarPropriedade(propriedade, vezDoJogador)
                if vezDoJogador.estaFalido():
                    self.removerAsPropriedades(vezDoJogador)

                retorno.append({
                    "tempo": "",
                    "nome": self.obterJogadorComMaiorSaldo().nome,
                    "saldo": self.obterJogadorComMaiorSaldo().saldo,
                    "posicao": self.obterJogadorComMaiorSaldo().posicao
                 })
        return self.__calcula(retorno)

    def obterJogadorComMaiorSaldo(self):
        return max(self.jogadores, key=lambda jogador: jogador.saldo)

    def removerAsPropriedades(self, vezDoJogador):
        propriedadesDoJogador = filter(lambda propriedade: propriedade.pertence(vezDoJogador), self.propriedades)
        for propriedade in propriedadesDoJogador:
            propriedade.proprietario = None

    def temQuePagarAluguel(self, propriedade, vezDoJogador):
        return propriedade.estaVendida() and propriedade.proprietario != vezDoJogador
    
    def comprarPropriedade(self, propriedade, vezDoJogador):
        propriedade.proprietario = vezDoJogador
        vezDoJogador.saldo -= propriedade.valorDaVenda

    def __rodarDado(self):
        return random.randint(1, 6)
    
    def pagarAluguel(self, propriedade, vezDoJogador):
        vezDoJogador.saldo -= propriedade.valorDoAluguel
        proprietario = self.obterJogador(propriedade.proprietario.nome)
        proprietario.saldo += propriedade.valorDoAluguel

    def obterJogador(self, nome):
        return next(jogador for jogador in self.jogadores if jogador.nome == nome)

    def __calcula(self, retorno):
        ganhadores = []
        comportamento_mais_vence = ''

        for resultados in retorno:
            ganhadores.append(resultados['nome'])

        impulsivo = ganhadores.count('Impulsivo')
        aleatorio = ganhadores.count('Aleatorio')
        exigente = ganhadores.count('Exigente')
        cauteloso = ganhadores.count('Cauteloso')

        if impulsivo > aleatorio and impulsivo > exigente and impulsivo > cauteloso:
            comportamento_mais_vence = 'Impulsivo'
        elif aleatorio > impulsivo and aleatorio > exigente and aleatorio > cauteloso:
            comportamento_mais_vence = 'Aleatorio'
        elif exigente > impulsivo and exigente > aleatorio and exigente > cauteloso:
            comportamento_mais_vence = 'Exigente'
        else:
            comportamento_mais_vence = 'Cauteloso'

        return {
            "quantidade": {
              "percentual": {
                  "impulsivo": '{0}%'.format((impulsivo/len(retorno)) * 100),
                  "aleatorio": '{0}%'.format((aleatorio/len(retorno)) * 100),
                  "exigente": '{0}%'.format((exigente/len(retorno)) * 100),
                  "cauteloso": '{0}%'.format((cauteloso/len(retorno)) * 100),
              },
              "comportamento_mais_vence": comportamento_mais_vence,
            },
            "resultados": retorno
        }


